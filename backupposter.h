#ifndef BACKUPPOSTER_H
#define BACKUPPOSTER_H
#include <QCoreApplication>
#include <QString>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include "backuplist.h"
#include <QSet>
#include <mutex>
#include <QAuthenticator>

class BackupPoster : public QCoreApplication
{
    Q_OBJECT
public:
    BackupPoster(QList<BackupList> backups, int argc, char* argv[]);

private:
    QList<BackupList> _backups;
    QSet<QString> _tracker;
    QSet<QNetworkReply*> _replies;
    QNetworkAccessManager *manager;

    std::mutex lock;

    void runBackups();
    void submitRequest(QString body, QString url);

private slots:
    void requestComplete(QNetworkReply *reply);
    void authRequired(QNetworkReply *reply, QAuthenticator *authenticator);
};

#endif // BACKUPPOSTER_H
