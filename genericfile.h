#ifndef GenericFile_H
#define GenericFile_H
#include "basebackup.h"
#include <QDateTime>
#include <QQueue>
#include <QStringList>
#include "offlinefileinfo.h"
#include "jsonbackupwriter.h"

class GenericFile : public BaseBackup, public JsonBackupWriter
{
public:
    GenericFile();
    GenericFile(QString name, QString backupLocation, int MinSize);
    QFileInfoList backupInfo();
    void updateFileInfo();
    QString toString();
    QDir backupDirectory();
    bool fileNotFound();
    void setFileNotFound(bool flag);
    QDateTime getLatestDateTime(QStringList &list);
    QDateTime getLatestDateTime(OfflineFileInfoList &list);
    QDateTime getLatestDateTime();
    unsigned long long getLargestFileSize(QStringList &list);
    unsigned long long getLargestFileSize(OfflineFileInfoList &list);
    unsigned long long getLargestFileSize();
    int countFilesR(QFileInfoList &list);
    int countFilesInDirs(QQueue<QDir> &queue);
    QStringList fileList();
    void setFileList(QStringList fileList);
    QStringList getRecursiveFileList(QFileInfoList &parent);
    int getTotalFileCount();
    unsigned long long getTotalFileSize();
    OfflineFileInfoList offlineFileInfoList();

protected:
    QDir _backupDirectory;
    QFileInfoList _backupInfo;
    bool _fileNotFound;
    QStringList _fileList;
    OfflineFileInfoList _offlineFileInfoList;
    OfflineFileInfoList getOfflineFileInfoList(QStringList list);
    const QJsonDocument JsonBackupStatus(QMap<QVariant, QVariant> *aux = 0) const;
    QString _typeName;
};

#endif // GenericFile_H
