#ifndef FILECOUNTSIZEPAIR_H
#define FILECOUNTSIZEPAIR_H

class FileCountSizePair
{
public:
    FileCountSizePair();
    unsigned long long size();
    unsigned int count();
    void setSize(unsigned long long size);
    void setCount(unsigned int count);

private:
    unsigned long long _size;
    unsigned int _count;
};

#endif // FILECOUNTSIZEPAIR_H
