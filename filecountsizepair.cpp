#include "filecountsizepair.h"

FileCountSizePair::FileCountSizePair()
{
}

unsigned long long FileCountSizePair::size() {
    return _size;
}

unsigned int FileCountSizePair::count() {
    return _count;
}

void FileCountSizePair::setSize(unsigned long long size) {
    _size = size;
}

void FileCountSizePair::setCount(unsigned int count) {
    _count = count;
}
