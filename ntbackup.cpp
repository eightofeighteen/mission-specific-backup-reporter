#include "ntbackup.h"

NTBackup::NTBackup()
{
}

NTBackup::NTBackup(QString name, QString backupLocation, int MinSize) {
    setName(name);
    setBackupLocation(backupLocation);
    setMinSize(MinSize);
}

QFileInfo NTBackup::backupInfo() {
    return _backupInfo;
}

void NTBackup::updateFileInfo() {
    _backupInfo.setFile(_backupLocation);
    if (!_backupInfo.exists())
        _backupStatus = FILENOTFOUND;
    else if (_backupInfo.lastModified() < QDateTime::currentDateTime().addDays(-maxBackupAge))
        _backupStatus = FILETOOOLD;
    else if ((_backupInfo.size() / 1024 / 1024) < _minSize)
        _backupStatus = FILETOOSMALL;
    else
        _backupStatus = PASSED;
}

QString NTBackup::toString() {
    QString out = "";
    out = _name + "\t" + _backupLocation + "\t" + QString::number(_minSize);
    return out;
}


