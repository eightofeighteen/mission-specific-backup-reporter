#include <QtCore/QCoreApplication>
#include <QtCore>
#include "basebackup.h"
#include "ntbackup.h"
#include "mssqldb.h"
#include "backuplist.h"
#include "htmlgraph/HTMLGraph.h"
#include "genericfile.h"
#include "backupposter.h"

//static QTextStream cout(stdout, QIODevice::WriteOnly);
//static QString endl = "\n";


namespace appInfo {
    static const QString appName = "Mission Specific Backup Reporter";
    static const QString version = "0.36 Alpha";
    static const QString buildDate = "20 August 2012";
    static const QString author = "Stuart Purnell";
    static const QString copyright = "Copyright 2012 Bytes Systems Integration (Pty) Ltd (see Licenses\\MSBR.txt for more information and see Licenses for additional licensing)";
    static const QString comments = "For internal use only.";
    static const bool verbose = true;
}

namespace HTMLPrefs {
    QString standardFont = "Times New Roman";
    int standardFontSize = 8;
    bool standardIsBold = false;
    bool standardIsItalized = false;
    QString standardColor = "000000";
    QString errorColor = "ff0000";
    QString standardBackground = "ffffff";
    QString errorBackground = "ffffff";
    bool errorIsBold = false;
    bool boldIsItalized = true;
    int headerFontSize = 12;
    bool headerIsBold = false;
    bool headerIsItalized = false;
    QString altStandardColor = "000000";
    QString altStandardBackground = "66CCFF";
    int tableWidth = 80;
    QString footerFont = "Time New Roman";
    int footerFontSize = 8;

}

BackupList office1;

void printVerbose(QString message) {
    if (appInfo::verbose)   cout << message << endl;
}

void getBackupInfo(QString input, QString &type, QString &name, QString &loc, QString &size) {
    QStringList list;
    list = input.split(",");
    type = list[0];
    name = list[1];
    loc = list[2];
    size = list[3];
}

void readBackupFile(QString fileName, BackupList &office) {
    QFile fin(fileName);
    if (!fin.open(QIODevice::ReadOnly | QIODevice::Text)) {
        cout << "Unable to open " << fileName << "." << endl;
        return;
    }
    QTextStream in(&fin);
    while (!in.atEnd()) {
        QString line = in.readLine();
        if (line.size() > 0) {
            QString bName = "";
            QString bLoc = "";
            QString bType = "";
            QString bSize = "";
            if (line.startsWith("NTB")) {
                getBackupInfo(line, bType, bName, bLoc, bSize);
                NTBackup singleBackup(bName, bLoc, bSize.toInt());
                office.NTBackups.append(singleBackup);
            } else if (line.startsWith("S")) {
                getBackupInfo(line, bType, bName, bLoc, bSize);
                Snapshot singleSnapshot(bName, bLoc, bSize.toInt());
                office.snapshots.append(singleSnapshot);
            } else if (line.startsWith("D")) {
                getBackupInfo(line, bType, bName, bLoc, bSize);
                MSSQLDB sqlDB(bName, bLoc, bSize.toInt());
                office.msdbbackups.append(sqlDB);
            } else if (line.startsWith("G")) {
                getBackupInfo(line, bType, bName, bLoc, bSize);
                GenericFile genericFile(bName, bLoc, bSize.toInt());
                office.genericFileBackups.append(genericFile);
            }
        }
    }
    office.updateFileInfo();
    for (auto &elem: office.snapshots) {
        cout << elem.toJson(&office.auxName()) << endl;
    }
    for (auto &elem: office.genericFileBackups) {
        cout << elem.toJson(&office.auxName()) << endl;
    }
    fin.close();
}

unsigned long long  printNTBackupReport(BackupList &office) {
    unsigned long long size = 0;
    for (int i = 0; i < office.NTBackups.size(); i++) {
        cout << "Name: " << office.NTBackups[i].name() << endl;
        //bool backupPassed = true;
        int backupStatus = 0;   // 0 passed, 1 file not found, 2 file too small, 3 date wrong
        if (!office.NTBackups[i].backupInfo().exists())
            backupStatus = 1;
        else if ((office.NTBackups[i].backupInfo().size() / 1024 / 1024) < office.NTBackups[i].minSize())
            backupStatus = 2;
        if (backupStatus == 0) {
            cout << "\tBackup passed." << endl;
            cout << "\tFile: " << office.NTBackups[i].backupInfo().fileName() << endl;
            cout << "\tSize: " << office.NTBackups[i].backupInfo().size() / 1024 / 1024 << endl;
            size += office.NTBackups[i].backupInfo().size();


        } else {
            cout << "\tBackup failed.  ";
            if (backupStatus == 1)
                cout << "File not found."<< endl;
            else if (backupStatus == 2)
                cout << "File size too small." << endl;
            else if (backupStatus == 3)
                cout << "File too old." << endl;
        }
    }
    return size;
}

unsigned long long printSnapshotReport(BackupList &office) {
    unsigned long long size = 0;
    for (int i = 0; i < office.snapshots.size(); i++) {
        cout << "Name: " << office.snapshots[i].name() << endl;
        int backupStatus = 0;
        if (office.snapshots[i].backupInfo().size() < 3)
            backupStatus = 1;
        if (backupStatus == 0) {
            cout << "\tBackup passed." << endl;
            QFileInfoList list = office.snapshots[i].backupInfo();
            for (int j = 0; j < list.size(); j++) {
                //cout << list.size() << endl;
                //cout << list[j].fileName() << endl;
                if (list[j].fileName() != "." && list[j].fileName() != ".." && !list[j].isDir()) {
                    cout << "\t" << list[j].fileName();
                    cout << "\t" << list[j].size() / 1024 / 1024 << endl;
                    size += list[j].size();
                }
            }
        }
    }
    return size;
}

void printReport(BackupList &office) {
    unsigned long long ntbSize = printNTBackupReport(office);
    cout << "Total Size: " << ntbSize / 1024 / 1024 << endl;
    unsigned long long snapshotSize = printSnapshotReport(office);
    cout << "Total Size: " << snapshotSize / 1024 / 1024 << endl;
    cout << "Grand Total Size: " << (ntbSize + snapshotSize) / 1024 / 1024 << endl;
}

QString getFileContents(QString fileName) {
    QString temp;

    QFile fin(fileName);
    if (!fin.open(QIODevice::ReadOnly | QIODevice::Text)) {
        cout << "Unable to open " << fileName << "." << endl;
        return QString("");
    }
    QTextStream in(&fin);
    while (!in.atEnd()) {
        QString line = in.readLine();
        temp += line;
    }
    //office.updateFileInfo();
    fin.close();
    return temp;
}

QString HTMLHeader() {
    QString temp = "";
    QString leftImage = getFileContents("left-image.txt");
    QString rightImage = getFileContents("right-image.txt");
    temp = temp + "<html>" + "\n";
    temp = temp + "<head>" + "\n";
    temp = temp + "<title>Backup Report for " + QDate::currentDate().addDays(-1).toString() + "</title>" + "\n";
    temp = temp + "<STYLE TYPE=\"text/css\">" + "\n";
    temp = temp + "<!--" + "\n";
    temp = temp + "TD{font-family: " + HTMLPrefs::standardFont + "; font-size: " + QString::number(HTMLPrefs::standardFontSize) + "pt; text-vertical-align: center}" + "\n";
    temp = temp + "h1{font-family: " + HTMLPrefs::standardFont + "; font-size: " + QString::number(HTMLPrefs::headerFontSize) + "pt; text-align: center; text-vertical-align: center}" + "\n";
    temp = temp + "p{font-family: " + HTMLPrefs::footerFont + "; font-size: " + QString::number(HTMLPrefs::footerFontSize) + "pt; text-align: left; text-vertical-align: bottom}" + "\n";
    temp = temp + "-->" + "\n";
    temp = temp + "</STYLE>" + "\n";
    temp = temp + "</head>" + "\n";
    temp = temp + "<body>" + "\n";
    temp = temp + "<table width=\"90%\" align=\"center\">";
    temp = temp + "<tr><td align=\"center\">" + leftImage + "</td><td align=\"center\">";
    temp = temp + "<h1>Backup Report for ";
    temp = temp + QDate::currentDate().addDays(-1).toString() + "</h1>" + "\n";
    temp = temp + "</td><td align=\"center\">" + rightImage + "</td></tr></table>";
    return temp;
}

QString HTMLFooter() {
    QString temp = "";
    //temp = temp + "Report generated at " + QDateTime::currentDateTime().toString() + ".<br>\n";
    temp = temp + "</body>" + "\n";
    temp = temp + "<footer style=\"font-family: " + HTMLPrefs::footerFont + "; font-size:" + QString::number(HTMLPrefs::footerFontSize) + "pt\">" + "<br>";
    temp = temp + "Report generated at " + QDateTime::currentDateTime().toString() + ".<br>\n";
    temp = temp + "" + appInfo::appName + " \n";
    temp = temp + "" + appInfo::version + "<br>\n";
    temp = temp + appInfo::comments + "<br>\n";

    temp = temp + "</footer>" + "\n";
    temp = temp + "</html>" + "\n";
    //cout << temp;
    return temp;
}

QString HTMLTableTop() {
    QString temp = "";
    temp = temp + "<table width=\"" + QString::number(HTMLPrefs::tableWidth) + "%\" cellspacing=\"0\" align=\"center\">" + "\n";
    return temp;
}

QString HTMLTableBottom() {
    QString temp = "";
    temp = temp + "</table>" + "\n";
    return temp;
}

QString newHTMLRow(QString backgroundColor = "ffffff") {
    QString temp = "";
    temp = temp + "<tr bgcolor=\"#" + backgroundColor + "\">" + "\n";
    return temp;
}

QString endHTMLRow() {
    QString temp = "";
    temp = temp + "</tr>" + "\n";
    return temp;
}

QString newHTMLCol(int align = 0) {
    QString temp = "";
    if (align == 0)
        temp = temp + "<td>" + "\n";
    else if (align == 1)
        temp = temp + "<td align=\"left\">" + "\n";
    else if (align == 2)
        temp = temp + "<td align=\"center\">" + "\n";
    else if (align == 3)
        temp = temp + "<td align=\"right\">" + "\n";
    return temp;
}

QString endHTMLCol() {
    QString temp = "";
    temp = temp + "</td>" + "\n";
    return temp;
}

/*QString HTMLRow(QString message, int span) {
    QString temp = "";
    temp = temp + "<td colspan=\"" + QString::number(span) + "\">" + message + "</td>" + "\n";
    return temp;
}*/

QString HTMLRow(QString message, int span = 0, int align = 0) {
    QString temp = "";
    if (span == 0) {
        if (align == 0)
            temp = temp + "<td>" + message + "</td>" + "\n";
        else if (align == 1)
            temp = temp + "<td>" + message + "</td>" + "\n";
        else if (align == 2)
            temp = temp + "<td>" + message + "</td>" + "\n";
        else if (align == 3)
            temp = temp + "<td>" + message + "</td>" + "\n";
    } else {
        if (align == 0)
            temp = temp + "<td colspan=\"" + QString::number(span) + "\">" + message + "</td>" + "\n";
        else if (align == 1)
            temp = temp + "<td colspan=\"" + QString::number(span) + "\" align=\"left\">" + message + "</td>" + "\n";
        else if (align == 2)
            temp = temp + "<td colspan=\"" + QString::number(span) + "\" align=\"center\">" + message + "</td>" + "\n";
        else if (align == 3)
            temp = temp + "<td colspan=\"" + QString::number(span) + "\" align=\"right\">" + message + "</td>" + "\n";
    }
    temp = temp + endHTMLRow();
    return temp;
}

QString HTMLRowN(QString message, int span = 0, int align = 0) {
    QString temp = "";
    if (span == 0) {
        if (align == 0)
            temp = temp + "<td>" + message + "</td>" + "\n";
        else if (align == 1)
            temp = temp + "<td>" + message + "</td>" + "\n";
        else if (align == 2)
            temp = temp + "<td>" + message + "</td>" + "\n";
        else if (align == 3)
            temp = temp + "<td>" + message + "</td>" + "\n";
    } else {
        if (align == 0)
            temp = temp + "<td colspan=\"" + QString::number(span) + "\">" + message + "</td>" + "\n";
        else if (align == 1)
            temp = temp + "<td colspan=\"" + QString::number(span) + "\" align=\"left\">" + message + "</td>" + "\n";
        else if (align == 2)
            temp = temp + "<td colspan=\"" + QString::number(span) + "\" align=\"center\">" + message + "</td>" + "\n";
        else if (align == 3)
            temp = temp + "<td colspan=\"" + QString::number(span) + "\" align=\"right\">" + message + "</td>" + "\n";
    }
    //temp = temp + endHTMLRow();
    return temp;
}

QString pretty(QString input) {
//    QString temp = "";
//    int count = 0;
//    QQueue<QString> stack;
//    bool nextSpace = false;
//    for (int i = 0; i < input.size(); i++) {
//        //count++;
//        stack.enqueue(QString(input[i]));
//    }
//    while (!stack.isEmpty()) {
//        count++;
//        if (count % 3 == 0)
//            nextSpace = true;
//        if (nextSpace) {
//            nextSpace = false;
//            temp += " ";
//        }
//        temp += stack.dequeue();
//    }

    int count = 0;
    QString temp = "";
    for (int i = input.size() - 1; i >= 0; i--) {
        temp += input[i];
        count++;
        if (count % 3 == 0)
            temp += " ";
    }

    QString temp2 = "";
    for (int i = temp.size() - 1; i >= 0; i--)
        temp2 += temp[i];

    return temp2;
}

QString HTMLNTBackupReport(BackupList &office, unsigned long long &outSize) {
    QString temp = "";
    unsigned long long size = 0;
    int cNum = 0;
    temp += "<tr bgcolor=\"#ffffff\">\n";
    temp += "<td style=\"border-bottom:solid 1px\" colspan=\"4\"><b>NTBackups</b>";
    temp += "</td></tr>\n";
    for (int i = 0; i < office.NTBackups.size(); i++) {
        cNum++;
        if (cNum % 2 == 1)
            temp = temp + newHTMLRow(HTMLPrefs::standardBackground);
        else
            temp = temp + newHTMLRow(HTMLPrefs::altStandardBackground);
        temp = temp + newHTMLCol();
        temp = temp + "<b>" + office.NTBackups[i].name() + "</b>" + "\n";
        temp = temp + endHTMLCol();
        /*int backupStatus = 0;   // 0 passed, 1 file not found, 2 file too small, 3 date wrong
        if (!office.NTBackups[i].backupInfo().exists())
            backupStatus = 1;
        else if ((office.NTBackups[i].backupInfo().size() / 1024 / 1024) < office.NTBackups[i].minSize())
            backupStatus = 2;*/
        if (office.NTBackups[i].backupStatus() == 0) {
            temp = temp + newHTMLCol();
            temp = temp + "Passed" + "\n";
            temp = temp + endHTMLCol();
            temp = temp + newHTMLCol();
            temp = temp + office.NTBackups[i].backupInfo().fileName() + "\n";
            temp = temp + endHTMLCol();
            temp = temp + newHTMLCol(3);
            temp = temp + pretty(QString::number(office.NTBackups[i].backupInfo().size() / 1024 / 1024)) + "\n";
            size += office.NTBackups[i].backupInfo().size();
            temp = temp + endHTMLCol();
            temp = temp + endHTMLRow();

        } else {
            temp = temp + newHTMLCol();
            temp = temp + "<font color=\"" + HTMLPrefs::errorColor + "\">" + "Failed" + "</td>\n";
            //temp = temp + endHTMLRow();
            //temp = temp + endHTMLCol();
            if (office.NTBackups[i].backupStatus() == 1)
                temp = temp + HTMLRowN("<font color=\"" + HTMLPrefs::errorColor + "\">File not found.</font>");
            else if (office.NTBackups[i].backupStatus() == 2)
                temp = temp + HTMLRowN("<font color=\"" + HTMLPrefs::errorColor + "\">File too small (" + QString::number(office.NTBackups[i].backupInfo().size() / 1024 / 1024) + "MB).</font>");
            else if (office.NTBackups[i].backupStatus() == 3)
                temp = temp + HTMLRowN("<font color=\"" + HTMLPrefs::errorColor + "\">File too old (" + office.NTBackups[i].backupInfo().created().toString() + ").</font>");
            temp = temp + "<td align=\"center\">-</td></tr>" + "\n";
        }
    }
    if (office.NTBackups.size() > 0) {
        temp += "<tr bgcolor=\"#ffffff\">\n";
        temp = temp + "<td style=\"border-top:solid 1px\" colspan=\"4\" align=\"right\"><b>" + pretty(QString::number(size / 1024 / 1024)) + "</b>";
        temp += "</td></tr>\n";
    } else {
        temp += "<tr bgcolor=\"#ffffff\">\n";
        temp = temp + "<td style=\"text-indent:2cm\" colspan=\"4\" align=\"left\"><i>No backups defined.</i>";
        temp += "</td></tr>\n";
    }
    outSize += size;
    return temp;
}

QString HTMLSnapshotReport(BackupList &office, unsigned long long &outSize) {
    QString temp = "";
    int cNum = 0;
    unsigned long long size = 0;
    temp += "<tr bgcolor=\"#ffffff\">\n";
    temp += "<td style=\"border-bottom:solid 1px\" colspan=\"4\"><b>Snapshots</b>";
    temp += "</td></tr>\n";
    for (int i = 0; i < office.snapshots.size(); i++) {
        cNum++;
        if (cNum % 2 == 1)
            temp = temp + newHTMLRow(HTMLPrefs::standardBackground);
        else
            temp = temp + newHTMLRow(HTMLPrefs::altStandardBackground);
        temp = temp + newHTMLCol();
        temp = temp + "<b>" + office.snapshots[i].name() + "</b>" + "\n";
        temp = temp + endHTMLCol();
        if (office.snapshots[i].backupStatus() == Snapshot::PASSED) {
            temp = temp + newHTMLCol();
            temp = temp + "Passed" + "\n";
            temp = temp + endHTMLCol();
            temp += newHTMLCol();
            temp += office.snapshots[i].backupLocation();
            temp += endHTMLCol();
            unsigned long long subSize = 0;
//            QFileInfoList list = office.snapshots[i].backupInfo();
//            for (int j = 0; j < list.size(); j++) {
//                if (list[j].fileName() != "." && list[j].fileName() != ".." && !list[j].isDir()) {
//                    size += list[j].size();
//                    subSize += list[j].size();
//                }
//            }

            OfflineFileInfoList list = office.snapshots[i].offlineFileInfoList();
            for (int j = 0; j < list.size(); j++) {
                size += list[j].size();
                subSize += list[j].size();
            }

            temp = temp + "<td align=\"right\">" + "\n";
            temp += pretty(QString::number(subSize / 1024 / 1024));
            temp += endHTMLCol();
            temp += endHTMLRow();
        } else {
            temp = temp + newHTMLCol();
            temp = temp + "<font color=\"" + HTMLPrefs::errorColor + "\">" + "Failed" + "\n";
            temp = temp + endHTMLCol();
            if (office.snapshots[i].backupStatus() == Snapshot::FILENOTFOUND)
                temp = temp + HTMLRowN("<font color=\"" + HTMLPrefs::errorColor + "\">File(s) not found.</font>");
            else if (office.snapshots[i].backupStatus() == Snapshot::FILETOOSMALL)
                temp = temp + HTMLRowN("<font color=\"" + HTMLPrefs::errorColor + "\">File(s) too small (" + QString::number(office.snapshots[i].getLargestFileSize()) + "MB).</font>");
            else if (office.snapshots[i].backupStatus() == Snapshot::FILETOOOLD)
                temp = temp + HTMLRowN("<font color=\"" + HTMLPrefs::errorColor + "\">File(s) too old (" + office.snapshots[i].getLatestDateTime().toString() + ").</font>");
            temp = temp + "<td align=\"center\">-</td></tr>" + "\n";
        }
    }
    if (office.snapshots.size() > 0) {
        temp += "<tr bgcolor=\"#ffffff\">\n";
        temp = temp + "<td style=\"border-top:solid 1px\" colspan=\"4\" align=\"right\"><b>" + pretty(QString::number(size / 1024 / 1024)) + "</b>";
        temp += "</td></tr>\n";
    } else {
        temp += "<tr bgcolor=\"#ffffff\">\n";
        temp = temp + "<td style=\"text-indent:2cm\" colspan=\"4\" align=\"left\"><i>No backups defined.</i>";
        temp += "</td></tr>\n";
    }
    outSize += size;
    return temp;
}

void textDBReport(BackupList office) {
    for (int i = 0; i < office.msdbbackups.size(); i++) {
        cout << office.msdbbackups[i].name() << endl;
        cout << office.msdbbackups[i].getTotalFileCount() << endl;
        cout << office.msdbbackups[i].getTotalFileSize() / 1024 / 1024 << "MB" << endl;
    }
}

QString changeSlant(QString line) {
    QString newLine = "";
    for (int i = 0; i < line.size(); i++) {
        if (line[i] == '/')
            newLine += "\\";
        else
            newLine += line[i];        
    }
    return newLine;
}

QString HTMLGenericFileReport(QList<GenericFile> &backup, unsigned long long &outSize, const QString &title = "Generic File Backups") {
    unsigned long long size = 0;
    int cNum = 0;
    QString temp = "";
    temp += "<tr bgcolor=\"#ffffff\">\n";
    temp += "<td style=\"border-bottom:solid 1px\" colspan=\"4\"><b>";
    temp += title;
    temp += "</b>";
    temp += "</td></tr>\n";
    QString colColor;

    for (int i = 0; i < backup.size(); i++) {
        cNum++;
        if (cNum % 2 == 1)
            colColor = HTMLPrefs::standardBackground;
        else
            colColor = HTMLPrefs::altStandardBackground;
        temp += newHTMLRow(colColor);
        temp = temp + newHTMLCol();
        temp = temp + "<b>" + backup[i].name() + "</b>" + "\n";
        temp = temp + endHTMLCol();
        if (backup[i].backupStatus() == MSSQLDB::PASSED) {
            temp += newHTMLCol();
            temp += "Passed";
            temp += endHTMLCol();
            temp += newHTMLCol();
            temp = temp + QString::number(backup[i].getTotalFileCount()) + " files.";
            temp += endHTMLCol();
            temp += "<td align=right><b>";
            temp += pretty(QString::number(backup[i].toMB(backup[i].getTotalFileSize())));
            temp += "</b>";
            size += backup[i].getTotalFileSize();
            temp += endHTMLCol();
            temp += endHTMLRow();
            for (int j = 0; j < backup[i].offlineFileInfoList().size(); j++) {
                cNum++;
                if (cNum % 2 == 1)
                    colColor = HTMLPrefs::standardBackground;
                else
                    colColor = HTMLPrefs::altStandardBackground;
                temp += newHTMLRow(colColor);
                temp += newHTMLCol();
                temp += endHTMLCol();
                temp += newHTMLCol();
                temp += endHTMLCol();
                temp += newHTMLCol();
                temp += "<i>";
                temp += changeSlant(backup[i].offlineFileInfoList()[j].filePath());
                temp += "</i>";
                temp += endHTMLCol();
                temp += "<td align=\"right\"><i>";temp += pretty(QString::number(backup[i].toMB(backup[i].offlineFileInfoList()[j].size())));
                temp += "</i>";
                temp += endHTMLCol();
                temp += endHTMLRow();
            }
        } else {
            temp += newHTMLCol();
            temp += "<font color=\"#" + HTMLPrefs::errorColor + "\">Failed</font>";
            temp += endHTMLCol();
            temp += newHTMLCol();
            if (backup[i].backupStatus() == MSSQLDB::FILENOTFOUND)
                temp += "<font color=\"#" + HTMLPrefs::errorColor + "\">File(s) not found.</font>";
            else if (backup[i].backupStatus() == MSSQLDB::FILETOOOLD)
                temp += "<font color=\"#" + HTMLPrefs::errorColor + "\">File(s) too old (" + backup[i].getLatestDateTime().toString() + ").</font>";
            else if (backup[i].backupStatus() == MSSQLDB::FILETOOSMALL)
                temp += "<font color=\"#" + HTMLPrefs::errorColor + "\">File(s) too small.</font>";
            temp += endHTMLCol();
            temp += newHTMLCol();
            temp += "<center>-</center>";
            temp += endHTMLCol();
            temp += endHTMLRow();

        }
    }
    if (backup.size() > 0) {
        temp += "<tr bgcolor=\"#ffffff\">\n";
        temp = temp + "<td style=\"border-top:solid 1px\" colspan=\"4\" align=\"right\"><b>" + pretty(QString::number(size / 1024 / 1024)) + "</b>";
        temp += "</td></tr>\n";
    } else {
        temp += "<tr bgcolor=\"#ffffff\">\n";
        temp = temp + "<td style=\"text-indent:2cm\" colspan=\"4\" align=\"left\"><i>No backups defined.</i>";
        temp += "</td></tr>\n";
    }
    outSize += size;
    return temp;
}

QString HTMLMSSQLReport(QList<MSSQLDB> &backup, unsigned long long &outSize) {
    QList<GenericFile> list;
    for (GenericFile &elem: backup)
        list.append(elem);
    return HTMLGenericFileReport(list, outSize, "Microsoft SQL Server DB Backups");
}

QString HTMLReport(BackupList &office) {
    QString temp = "";
    unsigned long long size = 0;
    temp = temp + "<tr><td style=\"font-family: " + HTMLPrefs::standardFont + "; font-size:" + QString::number(HTMLPrefs::standardFontSize + 2) + "pt\"><b>";
    temp = temp + "<a name=\"" + office.siteName + "\">";
    temp += office.siteName;
    temp += "</a></b></td></tr>";
    temp = temp + HTMLNTBackupReport(office, size);
    temp = temp + HTMLSnapshotReport(office, size);
    temp += HTMLMSSQLReport(office.msdbbackups, size);
    temp += HTMLGenericFileReport(office.genericFileBackups, size);
    temp = temp + "<td colspan=\"3\" style=\"border-bottom:solid 1px; border-top:solid 1px; text-vertical-align: center\" align=\"right\"><b>Total for " + office.siteName + "</b></td>";
    temp += "<td style=\"border-bottom:solid 1px; border-top:solid 1px; text-vertical-align: center\" align=\"right\">";
    temp += "<b>";
    temp += pretty(QString::number(size / 1024 / 1024));
    temp += "<b>";
    temp += endHTMLCol();
    temp += endHTMLRow();
    return temp;
}

int countPassedNTBackups(QList<NTBackup> &backups) {
    int count = backups.size();
    return count;
}

int countPassedSnapshotBackups(QList<Snapshot> &backups) {
    int count = 0;
    for (unsigned int i = 0; i < backups.size(); i++) {

    }
    return count;
}

QString HTMLContents(QList<BackupList> &officeList) {
    QString out = "";
    out += "<br><table width=\"80%\" align=\"center\">";
    for (int i = 0; i < officeList.size(); i++) {
        int passedNTBackups = countPassedNTBackups(officeList[i].NTBackups);
        int totalNTBackups = officeList[i].NTBackups.size();
        int passedSnapshotBackups = 0;
        int totalSnapshotBackups = officeList[i].snapshots.size();
        int passedDBBackups = 0;
        int totalDBBackups = 0;
        out += "<tr><td>";
        //out += out + QString::number(i + 1) + ".\t<a href=\"#" + officeList[i].siteName + "\">" + officeList[i].siteName + "</a></td></tr>\n";
        out += "<a href=\"#";
        out += officeList[i].siteName;
        out += "\">";
        out += QString::number(i + 1);
        out += "\t";
        out += officeList[i].siteName;
        out = out + " (" + QString::number(passedNTBackups) + "/" + QString::number(totalNTBackups) + ")";
        out = out + " (" + QString::number(passedSnapshotBackups) + "/" + QString::number(totalSnapshotBackups) + ")";
        out = out + " (" + QString::number(passedDBBackups) + "/" + QString::number(totalDBBackups) + ")";
        out += "</a>";
        out += "</td></tr>";
    }
    out += "</table><br>";
    return out;
}

void writeHTMLReport(BackupList &office, QString fileName) {
    QString out = "";
    QFile fout(fileName);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream outStream(&fout);
    out += HTMLHeader();
    //out += HTMLContents(office);
    out += HTMLTableTop();
    out += HTMLReport(office);
    out += HTMLTableBottom();
    out += HTMLFooter();
    //cout << out;
    outStream << out;
    fout.close();
}

void writeHTMLReport(QList<BackupList> &officeList, QString fileName) {
    QString out = "";
    //HTMLPrefs summPrefs("Times New Roman", 8, "blue", "red", "center", "center", "center", "right");
    QFile fout(fileName);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream outStream(&fout);
    out += HTMLHeader();
    out += HTMLContents(officeList);
    out += HTMLTableTop();
    for (unsigned int i = 0; i < officeList.size(); i++) {
        out += HTMLReport(officeList[i]);
    }
    out += HTMLTableBottom();
    out += HTMLFooter();
    outStream << out;
    fout.close();
}

QList<BackupList> populateList(QString info) {
    QList<BackupList> temp;

    QFile fin(info);
    if (!fin.open(QIODevice::ReadOnly | QIODevice::Text)) {
        cout << "Unable to open " << info << "." << endl;
        QList<BackupList> blankList;
        return blankList;
    }
    QTextStream in(&fin);
    while (!in.atEnd()) {
        QString line = in.readLine();
        if (line.size() > 0 && !line.startsWith("#") && !line.startsWith(";")) {
            QList<QString> input;
            input = line.split(",");
            QString siteName = input[0];
            QString siteFile = input[1];
            BackupList backupOffice;
            backupOffice.siteName = siteName;
            if (!siteName.startsWith("#")) {
                printVerbose("Getting file info for " + siteName + ".");
                readBackupFile(siteFile, backupOffice);
                temp.append(backupOffice);
            }
        }
    }
    //office.updateFileInfo();
    fin.close();
    return temp;
}

QList<BackupList> processBackups(QString info) {
    printVerbose("Reading site list " + info + ".");
    QList<BackupList> mainList;
    mainList = populateList(info);
    //for (unsigned int i = 0; i < mainList.size(); i++)
    //    writeHTMLReport(mainList[i], mainList[i].siteName + ".html");
    QString reportFileName = "backup-report.html";
    printVerbose("Generating report to " + reportFileName + ".");
    writeHTMLReport(mainList, reportFileName);
    return mainList;
}

int main(int argc, char *argv[])
{
    printVerbose(appInfo::appName + " " + appInfo::version);
    auto backups = processBackups("sites.txt");

    BackupPoster app(backups, argc, argv);
    return app.exec();
    //return 0;
}
