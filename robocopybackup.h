#ifndef ROBOCOPYBACKUP_H
#define ROBOCOPYBACKUP_H
#include "basebackup.h"
#include "offlinefileinfo.h"
#include "filecountsizepair.h"
#include <QDateTime>
#include <QString>
#include <QList>

class RoboCopyBackup : public BaseBackup
{
public:
    RoboCopyBackup();
};

#endif // ROBOCOPYBACKUP_H
