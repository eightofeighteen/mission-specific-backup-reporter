#include "backuplist.h"

/*!
  Default constructor.  Does nothing.
  */
BackupList::BackupList()
{
}

/*!
  Calls updateFileInfo() from all the elements of NTBackups, snapshots and msdbbackups.
  */
void BackupList::updateFileInfo() {
    for (int i = 0; i < NTBackups.size(); i++)
        NTBackups[i].updateFileInfo();
    for (int i = 0; i < snapshots.size(); i++)
        snapshots[i].updateFileInfo();
    for (int i = 0; i < msdbbackups.size(); i++)
        msdbbackups[i].updateFileInfo();
    for (GenericFile &elem: genericFileBackups) {
        elem.updateFileInfo();
    }
}

QMap<QVariant, QVariant> BackupList::auxName() const {
    QVariant title = QString("siteName");
    QVariant name = siteName;
    QMap<QVariant, QVariant> m;
    m.insert(title, name);
    return m;
}
