#ifndef BASEBACKUP_H
#define BASEBACKUP_H

#include <QString>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QDirIterator>
#include <QDebug>
#include <QMap>
#include <QVariant>

static QTextStream cout(stdout, QIODevice::WriteOnly);

class BaseBackup
{
public:
    BaseBackup();
    BaseBackup(QString name, QString backupLocation, int minSize);
    QString name();
    QString backupLocation();
    int minSize();
    void setName(QString name);
    void setBackupLocation(QString backupLocation);
    void setMinSize(int size);
    const QString backupStrStatus() const;
    static const int maxBackupAge = 1;
    static const int PASSED = 0;
    static const int FILENOTFOUND = 1;
    static const int FILETOOSMALL = 2;
    static const int FILETOOOLD = 3;

    int backupStatus();
    void setBackupStatus(int status);
    unsigned long long toMB(unsigned long long size) const;
    unsigned long long toGB(unsigned long long size) const;

protected:
    QString _name;
    QString _backupLocation;
    int _minSize;
    int _backupStatus;
};

#endif // BASEBACKUP_H
