#ifndef SNAPSHOT_H
#define SNAPSHOT_H
#include "basebackup.h"
#include "offlinefileinfo.h"
#include <QDateTime>
#include "jsonbackupwriter.h"

class Snapshot : public BaseBackup, public JsonBackupWriter
{
public:
    Snapshot();
    Snapshot(QString name, QString backupLocation, int MinSize);
    QFileInfoList backupInfo();
    void updateFileInfo();
    QString toString();
    QDir backupDirectory();
    bool fileNotFound();
    void setFileNotFound(bool flag);
    QDateTime getLatestDateTime(QFileInfoList &list);
    QDateTime getLatestDateTime(OfflineFileInfoList &list);
    QDateTime getLatestDateTime();
    unsigned long long getLargestFileSize(QFileInfoList &list);
    unsigned long long getLargestFileSize(OfflineFileInfoList &list);
    unsigned long long getLargestFileSize();
    static const int maxBackupAge = 7;
    OfflineFileInfoList offlineFileInfoList();

private:
    QDir _backupDirectory;
    QFileInfoList _backupInfo;
    bool _fileNotFound;
    OfflineFileInfoList _offlineFileInfoList;
    const QJsonDocument JsonBackupStatus(QMap<QVariant, QVariant> *aux = 0) const;
};

#endif // SNAPSHOT_H
