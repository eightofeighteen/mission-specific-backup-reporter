#ifndef MSSQLDB_H
#define MSSQLDB_H
#include "basebackup.h"
#include <QDateTime>
#include <QQueue>
#include <QStringList>
#include "offlinefileinfo.h"
#include "genericfile.h"

class MSSQLDB : public GenericFile
{
public:
    MSSQLDB();
    MSSQLDB(QString name, QString backupLocation, int MinSize);

private:

};

#endif // MSSQLDB_H
