#include "mssqldb.h"

MSSQLDB::MSSQLDB() : GenericFile()
{
}

MSSQLDB::MSSQLDB(QString name, QString backupLocation, int MinSize) : GenericFile(name, backupLocation, MinSize)
{
    _typeName = "Microsoft SQL Server DB Backup";
}
