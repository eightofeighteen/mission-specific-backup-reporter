#ifndef JSONBACKUPWRITER_H
#define JSONBACKUPWRITER_H
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QMap>
#include <QVariant>

class JsonBackupWriter
{
public:
    JsonBackupWriter();
    JsonBackupWriter(QJsonDocument::JsonFormat jsonFormat);
    const QByteArray toJson(QMap<QVariant, QVariant> *aux = 0) const;

protected:
    const virtual QJsonDocument JsonBackupStatus(QMap<QVariant, QVariant> *aux = 0) const = 0;
    QJsonDocument::JsonFormat JsonFormat;
};

#endif // JSONBACKUPWRITER_H
