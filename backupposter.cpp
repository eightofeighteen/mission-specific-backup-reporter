#include "backupposter.h"

BackupPoster::BackupPoster(QList<BackupList> backups, int argc, char *argv[]) : QCoreApplication(argc, argv)
{
    _backups = backups;
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(requestComplete(QNetworkReply*)));
    connect(manager, SIGNAL(authenticationRequired(QNetworkReply*,QAuthenticator*)), this, SLOT(authRequired(QNetworkReply*,QAuthenticator*)));

    runBackups();
}

void BackupPoster::runBackups()
{
    cout << "Running backups." << endl;
    for (auto &site: _backups)
    {
        for (auto &elem: site.snapshots)
        {
            submitRequest(elem.toJson(&site.auxName()), "http://hoapp1.za.illovo.net/hc-dev/api/SnapshotBackup");
        }
        for (auto &elem: site.genericFileBackups)
            submitRequest(elem.toJson(&site.auxName()), "http://hoapp1.za.illovo.net/hc-dev/api/genericfilebackup");
    }
}

void BackupPoster::submitRequest(QString body, QString url)
{
    QNetworkRequest request = QNetworkRequest(QUrl(url));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    lock.lock();
    _replies.insert(manager->post(request, body.toLocal8Bit()));
    lock.unlock();
}

void BackupPoster::requestComplete(QNetworkReply *reply)
{
    lock.lock();
    if (_replies.contains(reply))
    {
        _replies.remove(reply);
    }

    reply->deleteLater();
    if (_replies.count() == 0)
        emit quit();
    lock.unlock();
}

void BackupPoster::authRequired(QNetworkReply *reply, QAuthenticator *authenticator)
{
    cout << "Auth required." << endl;
}
