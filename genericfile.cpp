#include "GenericFile.h"

GenericFile::GenericFile()
{
}

QFileInfoList GenericFile::backupInfo() {
    return _backupInfo;
}

int GenericFile::countFilesInDirs(QQueue<QDir> &queue) {
    int count = 0;
    QQueue<QDir> dirs = queue;
    while (!dirs.isEmpty()) {
        QDir dir = dirs.dequeue();
        QFileInfoList files = dir.entryInfoList();
        for (int i = 0; i < files.size(); i++)
            if (!files[i].isDir())
                count++;
    }
    return count;
}

int GenericFile::countFilesR(QFileInfoList &list) {
    int count = 0;
    QQueue<QDir> queue;
    for (int i = 0; i < list.size(); i++) {
        if (list[i].isDir())
            queue.enqueue(QDir(list[i].filePath()));
    }
    count = countFilesInDirs(queue);
    return count;
}

GenericFile::GenericFile(QString name, QString backupLocation, int MinSize) {
    setName(name);
    setBackupLocation(backupLocation);
    setMinSize(MinSize);
    _typeName = "Generic File Backup";
    _fileNotFound = false;
    if (!_backupDirectory.cd(_backupLocation))
            _fileNotFound = true;
    else {
        QFileInfoList list = _backupDirectory.entryInfoList();
        if (list.size() < 3)
            _fileNotFound = true;
        if (countFilesR(list) == 0)
            _fileNotFound = true;
    }
}

QDateTime GenericFile::getLatestDateTime(QStringList &list) {
    QDateTime current;
    //qDebug() << "SQL getLatestDateTime";
    if (list.size() > 0) {
        QFileInfo info(list[0]);
        current = info.lastModified();
        for (int i = 0; i < list.size(); i++) {
            //qDebug() << list[i];
            QFileInfo info(list[i]);
            if (info.lastModified() > current)
                current = info.lastModified();
        }
    }
    //qDebug() << QString("Returning " + current.toString());
    return current;
}

QDateTime GenericFile::getLatestDateTime(OfflineFileInfoList &list) {
    QDateTime current;
    //qDebug() << "SQL getLatestDateTime";
    if (list.size() > 0) {
        OfflineFileInfo info = list[0];
        current = info.lastModified();
        for (int i = 0; i < list.size(); i++) {
            //qDebug() << list[i];
            OfflineFileInfo info = list[i];
            if (info.lastModified() > current)
                current = info.lastModified();
        }
    }
    //qDebug() << QString("Returning " + current.toString());
    return current;
}

QDateTime GenericFile::getLatestDateTime() {
    //return getLatestDateTime(_fileList);
    return getLatestDateTime(_offlineFileInfoList);
}

unsigned long long GenericFile::getLargestFileSize(QStringList &list) {
    unsigned long long size = 0;
    for (int i = 0; i < list.size(); i++) {
        QFileInfo info(list[i]);
        size += info.size();
    }
    return size;
}

unsigned long long GenericFile::getLargestFileSize(OfflineFileInfoList &list) {
    unsigned long long size = 0;
    for (int i = 0; i < list.size(); i++) {
        OfflineFileInfo info = list[i];
        size += info.size();
    }
    return size;
}

unsigned long long GenericFile::getLargestFileSize() {
    //return getLargestFileSize(_fileList);
    return getLargestFileSize(_offlineFileInfoList);
}

QStringList GenericFile::getRecursiveFileList(QFileInfoList &parent) {
//    qDebug() << "Starting.";
//    qDebug() << "Cleared.";
    QStringList list;
    QQueue<QDir> queue;
    for (int i = 0; i < parent.size(); i++)
        if (parent[i].isDir() && parent[i].fileName() != "..") {
//            if (parent[i].fileName() != ".")
//                queue.enqueue(QDir(parent[i].filePath().left(parent[i].filePath().size() - 2)));
//            else
                queue.enqueue(QDir(parent[i].filePath()));
        }
    while (!queue.isEmpty()) {
        QDir dir = queue.dequeue();
        QFileInfoList files = dir.entryInfoList();
        for (int i = 0; i < files.size(); i++)
            if (!files[i].isDir())
                list.append(files[i].filePath());
    }
//    for (int i = 0; i < list.size(); i++)
//        qDebug() << list[i];
    //qDebug() << "Size before return is " << list.size();
    //qDebug() << "About to return.";
    return list;
}

OfflineFileInfoList GenericFile::getOfflineFileInfoList(QStringList list) {
    OfflineFileInfoList infoList;
    for (int i = 0; i < list.size(); i++) {
        QFileInfo file(list[i]);
        OfflineFileInfo info;
        info.setFileName(file.fileName());
        info.setFilePath(file.filePath());
        info.setCreated(file.created());
        info.setLastModified(file.lastModified());
        info.setLastAccessed(file.lastRead());
        info.setSize(file.size());
        //info.setSize(toMB(file.size()));
        infoList.append(info);
    }
    return infoList;
}

void GenericFile::updateFileInfo() {
    _backupDirectory.cd(_backupLocation);
    //qDebug() << "Backup location is " << _backupLocation;
    _backupInfo = _backupDirectory.entryInfoList();
    //qDebug() << "Size is " << _backupInfo.size();

    //QFileInfoList _fileListTemp = getRecursiveFileList(_backupInfo);
    //qDebug() << "Returned.";

    //QStringList _fileListTemp = getRecursiveFileList(_backupInfo);
    _fileList = getRecursiveFileList(_backupInfo);
    _offlineFileInfoList = getOfflineFileInfoList(_fileList);

    //_fileList.append(_fileListTemp);
    //qDebug() << "Size is " << _fileListTemp.size();
    //getRecursiveFileList(_backupInfo, _fileList);
    //qDebug() << "done.";

    if (_fileList.size() == 0)
        _backupStatus = FILENOTFOUND;
    else {
        if (getLatestDateTime() < QDateTime::currentDateTime().addDays(-maxBackupAge))
            _backupStatus = FILETOOOLD;
        else if (getLargestFileSize() < (_minSize * 1024 * 1024))
            _backupStatus = FILETOOSMALL;
        else
            _backupStatus = PASSED;
    }

    /*if (!_backupDirectory.cd(_backupLocation))
        _backupStatus = FILENOTFOUND;
    else {
        QFileInfoList list = _backupDirectory.entryInfoList();
        if (list.size() < 3)
            _backupStatus = FILENOTFOUND;
        else if (getLatestDateTime(list) < QDateTime::currentDateTime().addDays(-maxBackupAge))
            _backupStatus = FILETOOOLD;
        else if (getLargestFileSize(list) < (_minSize * 1024 * 1024))
            _backupStatus = FILETOOSMALL;
        else
            _backupStatus = PASSED;
    }*/

    /*for (unsigned int i = 0; i < offlineFileInfoList().size(); i++)
        qDebug() << offlineFileInfoList()[i].filePath() << endl;    */
}

int GenericFile::getTotalFileCount() {
    //return _fileList.size();
    return _offlineFileInfoList.size();
}

unsigned long long GenericFile::getTotalFileSize() {
    unsigned long long size = 0;
//    for (int i = 0; i < _fileList.size(); i++) {
//        QFileInfo file(_fileList[i]);
//        size += file.size();
//    }
    for (int i = 0; i < _offlineFileInfoList.size(); i++) {
        size += _offlineFileInfoList[i].size();
    }
    return size;
}

QString GenericFile::toString() {
    QString out = "";
    out = _name + "\t" + _backupLocation + "\t" + QString::number(_minSize);
    return out;
}

QDir GenericFile::backupDirectory() {
    return _backupDirectory;
}

bool GenericFile::fileNotFound() {
    return _fileNotFound;
}

void GenericFile::setFileNotFound(bool flag) {
    _fileNotFound = flag;
}

QStringList GenericFile::fileList() {
    return _fileList;
}

void GenericFile::setFileList(QStringList fileList) {
    _fileList = fileList;
}

OfflineFileInfoList GenericFile::offlineFileInfoList() {
    return _offlineFileInfoList;
}

const QJsonDocument GenericFile::JsonBackupStatus(QMap<QVariant, QVariant> *aux) const {
    QString typeName = _typeName;

    QJsonObject jo;
    jo["typeName"] = typeName;
    jo["backupName"] = _name;
    jo["status"] = _backupStatus == PASSED ? "Passed" : "Failed";
    jo["extendedstatus"] = backupStrStatus();
    jo["size"] = QString::number(OfflineFileInfo::totalSize(_offlineFileInfoList));
    jo["path"] = _backupDirectory.path();

    auto fileList = _offlineFileInfoList;

    QJsonArray ja;


    for (auto &elem: fileList) {
        QJsonObject fileObject;
        fileObject["filename"] = elem.fileName();
        fileObject["filesize"] = QString::number(elem.size());
        fileObject["lastmodified"] = elem.lastModified().toSecsSinceEpoch();
        QJsonValue val(fileObject);
        ja.append(val);
    }

    jo["filelist"] = ja;

    if (aux != 0) {
        for (auto &elem: aux->keys())
            jo[elem.toString()] = (aux->value(elem)).toString();
    }

    QJsonDocument doc(jo);
    return doc;
}
