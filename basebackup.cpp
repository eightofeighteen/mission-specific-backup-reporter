#include "basebackup.h"

/*!
  Defualt initiator.  Does nothing.
  */
BaseBackup::BaseBackup()
{
}

/*!
  Overloaded initiator.
  QString name: name of the backup host.
  QString backupLocation: physical location of the backup.
  int minSize: smallest file size which will mark the backup as successful.
  */
BaseBackup::BaseBackup(QString name, QString backupLocation, int minSize) {
    _name = name;
    _backupLocation = backupLocation;
    _minSize = minSize;
}

/*!
  Getter which returns the backup host.
  */
QString BaseBackup::name() {
    return _name;
}

/*!
  Getter which returns the physical location of the backup.
  */
QString BaseBackup::backupLocation() {
    return _backupLocation;
}

/*!
  Getter which returns the smallest file size which will mark the backup as successful.
  */
int BaseBackup::minSize() {
    return _minSize;
}

/*!
  Setter to set the backup host.
  */
void BaseBackup::setName(QString name) {
    _name = name;
}

/*!
  Setter to set the physical backup location.
  QString backupLocation: physical backup location.
  */
void BaseBackup::setBackupLocation(QString backupLocation) {
    _backupLocation = backupLocation;
}

/*!
  Setter to set the smallest file size which will mark the backup as successful.
  int size: smallest file size which will mark the backup as successful.
  */
void BaseBackup::setMinSize(int size) {
    _minSize = size;
}

/*!
  Getter which returns an integer representing the backup status.
  Ref:
    static const int maxBackupAge = 1;
    static const int PASSED = 0;
    static const int FILENOTFOUND = 1;
    static const int FILETOOSMALL = 2;
    static const int FILETOOOLD = 3;
  */
int BaseBackup::backupStatus() {
    return _backupStatus;
}

/*!
  Setter to set the backup status.
  int status: integer representation of the backup status.
  Ref:
    static const int maxBackupAge = 1;
    static const int PASSED = 0;
    static const int FILENOTFOUND = 1;
    static const int FILETOOSMALL = 2;
    static const int FILETOOOLD = 3;
  */
void BaseBackup::setBackupStatus(int status) {
    _backupStatus = status;
}

/*!
  Converts a value assumed to be in bytes to MB.  Rounds up or down.
  */
unsigned long long BaseBackup::toMB(unsigned long long size) const {
    unsigned long long out = 0;
    out = size / 1024 / 1024;
    unsigned int diff = size - (out * 1024 * 1024);
    int mb = 1024 * 1024;
    if (diff >= (mb / 2))
        out++;
    //qDebug() << "Input is " << QString::number(size) << ", 1out is " << QString::number(size / 1024 / 1024)
    //         << ", diff is " << QString::number(diff) << ", one is " << QString::number(mb) << ", halfway is " << QString::number(mb / 2)
    //         << ", return is " << QString::number(out) << ".";
    return out;
}

/*!
  Converts a value assumed to be in bytes to GB.  Rounds up or down.
  */
unsigned long long BaseBackup::toGB(unsigned long long size) const {
    unsigned long long out = 0;
    out = size / 1024 / 1024 / 1024;
    unsigned int diff = size - (out * 1024 * 1024 * 1024);
    int gb = 1024 * 1024 * 1024;
    if (diff >= (gb / 2))
        out++;
    return out;
}

/*!
 * \brief BaseBackup::backupStrStatus
 * \return
 */
const QString BaseBackup::backupStrStatus() const {
    QString label = "";
    switch (_backupStatus) {
    case PASSED:
        label = "Passed";
        break;
    case FILENOTFOUND:
        label = "File not found";
        break;
    case FILETOOSMALL:
        label = "File too small";
        break;
    case FILETOOOLD:
        label = "File too old";
        break;
    default:
        break;
    }
    return label;
}
