#include "offlinefileinfo.h"

OfflineFileInfo::OfflineFileInfo()
{
}

OfflineFileInfo::OfflineFileInfo(QString fileName, QString filePath, QDateTime created, QDateTime lastModified, QDateTime lastAccessed) {
    _fileName = fileName;
    _filePath = filePath;
    _created = created;
    _lastModified = lastModified;
    _lastAccessed = lastAccessed;
}

QString OfflineFileInfo::fileName() {
    return _fileName;
}

QString OfflineFileInfo::filePath() {
    return _filePath;
}

QDateTime OfflineFileInfo::created() {
    return _created;
}

QDateTime OfflineFileInfo::lastModified() {
    return _lastModified;
}

QDateTime OfflineFileInfo::lastAccessed() {
    return _lastAccessed;
}

void OfflineFileInfo::setFileName(QString fileName) {
    _fileName = fileName;
}

void OfflineFileInfo::setFilePath(QString filePath) {
    _filePath = filePath;
}

void OfflineFileInfo::setCreated(QDateTime created) {
    _created = created;
}

void OfflineFileInfo::setLastModified(QDateTime lastModified) {
    _lastModified = lastModified;
}

void OfflineFileInfo::setLastAccessed(QDateTime lastAccessed) {
    _lastAccessed = lastAccessed;
}

const unsigned long long OfflineFileInfo::size() const {
    return _size;
}

void OfflineFileInfo::setSize(unsigned long long size) {
    _size = size;
}

unsigned long long OfflineFileInfo::totalSize(const OfflineFileInfoList &list) {
    unsigned long long count = 0;
    for (auto &elem: list)
        count += elem.size();
    return count;
}
