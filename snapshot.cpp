#include "snapshot.h"

Snapshot::Snapshot()
{
}

Snapshot::Snapshot(QString name, QString backupLocation, int MinSize) {
    setName(name);
    setBackupLocation(backupLocation);
    setMinSize(MinSize);
    _fileNotFound = false;
    if (!_backupDirectory.cd(_backupLocation))
            _fileNotFound = true;
    else {
        QFileInfoList list = _backupDirectory.entryInfoList();
        if (list.size() < 3)
            _fileNotFound = true;
    }
    /*for (QFileInfo &elem: _backupDirectory.entryInfoList()) {
        cout << elem.fileName() << "\t";
        cout << elem.size() << endl;
    }*/
    //cout << getLatestDateTime().toString() << endl;
}

QFileInfoList Snapshot::backupInfo() {
    return _backupInfo;
}

QDateTime Snapshot::getLatestDateTime(QFileInfoList &list) {
    QDateTime current = QDateTime::currentDateTime();
    for (int i = 0; i < list.size(); i++) {
        if (list[i].fileName().toLower().endsWith(".sna"))
            current = list[i].lastModified();
    }
    return current;
}

QDateTime Snapshot::getLatestDateTime(OfflineFileInfoList &list) {
    QDateTime current = QDateTime::currentDateTime();
    for (int i = 0; i < list.size(); i++) {
        if (list[i].fileName().toLower().endsWith(".sna"))
            current = list[i].lastModified();
    }
    return current;
}

QDateTime Snapshot::getLatestDateTime() {
    return getLatestDateTime(_offlineFileInfoList);
}

unsigned long long Snapshot::getLargestFileSize(QFileInfoList &list) {
    unsigned long long size = 0;
    for (int i = 0; i < list.size(); i++)
        if (list[i].fileName().toLower().endsWith(".sna"))
            if (size < list[i].size())
                size = list[i].size();
    return size;
}

unsigned long long Snapshot::getLargestFileSize(OfflineFileInfoList &list) {
    unsigned long long size = 0;
    for (int i = 0; i < list.size(); i++)
        if (list[i].fileName().toLower().endsWith(".sna"))
            if (size < list[i].size())
                size = list[i].size();
    return size;
}

unsigned long long Snapshot::getLargestFileSize() {
    return getLargestFileSize(_offlineFileInfoList);
}

void Snapshot::updateFileInfo() {
    _backupInfo = _backupDirectory.entryInfoList();
    if (!_backupDirectory.cd(_backupLocation))
        _backupStatus = FILENOTFOUND;
    else {
        QFileInfoList list = _backupDirectory.entryInfoList();
        if (list.size() < 3)
            _backupStatus = FILENOTFOUND;
        else if (getLatestDateTime(list) < QDateTime::currentDateTime().addDays(-maxBackupAge))
            _backupStatus = FILETOOOLD;
        else if (getLargestFileSize(list) < (_minSize * 1024 * 1024))
            _backupStatus = FILETOOSMALL;
        else
            _backupStatus = PASSED;
    }
    if (_backupStatus == PASSED) {

    }
    for (unsigned int i = 0; i < _backupInfo.size(); i++) {
        if (_backupInfo[i].fileName() != "." && _backupInfo[i].fileName() != ".." && !_backupInfo[i].isDir()) {
            OfflineFileInfo file;
            file.setSize(_backupInfo[i].size());
            file.setCreated(_backupInfo[i].created());
            file.setLastModified(_backupInfo[i].lastModified());
            file.setFileName(_backupInfo[i].fileName());
            file.setFilePath(_backupInfo[i].filePath());
            file.setLastAccessed(_backupInfo[i].lastRead());
            _offlineFileInfoList.push_back(file);
        }
    }
}

QString Snapshot::toString() {
    QString out = "";
    out = _name + "\t" + _backupLocation + "\t" + QString::number(_minSize);
    return out;
}

QDir Snapshot::backupDirectory() {
    return _backupDirectory;
}

bool Snapshot::fileNotFound() {
    return _fileNotFound;
}

void Snapshot::setFileNotFound(bool flag) {
    _fileNotFound = flag;
}

OfflineFileInfoList Snapshot::offlineFileInfoList() {
    return _offlineFileInfoList;
}

const QJsonDocument Snapshot::JsonBackupStatus(QMap<QVariant, QVariant> *aux) const {
    QString typeName = "Snapshot";

    QJsonObject jo;
    jo["typeName"] = typeName;
    jo["backupName"] = _name;
    jo["status"] = _backupStatus == PASSED ? "Passed" : "Failed";
    jo["extendedstatus"] = backupStrStatus();
    jo["size"] = QString::number(OfflineFileInfo::totalSize(_offlineFileInfoList));
    jo["path"] = _backupDirectory.path();

    if (aux != 0) {
        for (auto &elem: aux->keys())
            jo[elem.toString()] = (aux->value(elem)).toString();
    }

    QJsonDocument doc(jo);
    return doc;
}
