//
//  HTMLGraphPrefs.cpp
//  HTMLTest
//
//  Created by Stuart Purnell on 2012/09/26.
//  Copyright (c) 2012 Stuart Purnell. All rights reserved.
//

#include "HTMLGraphPrefs.h"

HTMLGraphPrefs::HTMLGraphPrefs() {
    
}

HTMLGraphPrefs::HTMLGraphPrefs(std::string font, int fontSize, std::string colorLeft, std::string colorRight, std::string alignVertical, std::string alignHorizontal, std::string alignVerticalLabel, std::string alignHorizontalLabel) {
    this->font = font;
    this->fontSize = fontSize;
    this->colorLeft = colorLeft;
    this->colorRight = colorRight;
    this->alignVertical = alignVertical;
    this->alignHorizontal = alignHorizontal;
    this->alignVerticalLabel = alignVerticalLabel;
    this->alignHorizontalLabel = alignHorizontalLabel;
}
