//
//  HTMLGraph.cpp
//  HTMLTest
//
//  Created by Stuart Purnell on 2012/09/26.
//  Copyright (c) 2012 Stuart Purnell. All rights reserved.
//

#include "HTMLGraph.h"

HTMLGraph::HTMLGraph() {
    
}

std::string HTMLGraph::int2str(int num) {
    std::stringstream temp;
    temp << num;
    return temp.str();
}

std::string HTMLGraph::HTMLHeader() {
    std::string el = "\n";
    std::string out = "<html>" + el + "<head>" + el + "<title>Test</title>" + el + "</head>" + el + "<body>" + el;
    return out;
}

std::string HTMLGraph::HTMLFooter() {
    std::string el = "\n";
    std::string out = "</body>" + el + "</html>" + el;
    return out;
}

std::string HTMLGraph::outputHTMLGraph(GraphPairList & graphData, HTMLGraphPrefs & prefs) {
    std::string el = "\n";
    //std::string out = "<table width=\"80%\" align=\"center\" border=\"0\" cellpadding=\"0\">" + el;
    std::string out = "<table style=\"width: 100%; border-spacing: 1pt\">" + el;
    std::string newRow = "<tr>" + el;
    for (int j = 0; j < 120; j++)
        newRow = newRow + "<td width=\"1\"></td>" + el;
    newRow = newRow + "</tr>" + el;
    out += newRow;
    for (int i = 0; i < graphData.size(); i++) {
        //int value = rand() % 100 + 1;
        int value = graphData[i].valueP;
        std::string newRow = "<tr height=\"8\">" + el;
        newRow = newRow + "<td colspan=\"20\" style=\"text-align:" + prefs.alignHorizontalLabel + ";\">" + graphData[i].title + "</td>" + el;
        //newRow = newRow + "<td colspan=\"" + int2str(value) + "\" bgcolor=\"#ff0000\" align=\"center\">" + int2str(value) + "</td>" + el;
        //newRow = newRow + "<td colspan=\"" + int2str(100 - value) + "\" bgcolor=\"#0000ff\" align=\"center\">" + int2str(100 - value) + "</td>" + el;
        if (value > 0)
            newRow = newRow + "<td colspan=\"" + int2str(value) + "\" style=\"background: " + prefs.colorLeft + "; text-align: " + prefs.alignHorizontal + "; vertical-align: " + prefs.alignVertical + "; font-size:" + int2str(prefs.fontSize) + "pt; color:white\">" + int2str(value) + "</td>" + el;
        if (100 - value > 0)
            newRow = newRow + "<td colspan=\"" + int2str(100 - value) + "\" style=\"background: " + prefs.colorRight + "; text-align: " + prefs.alignHorizontal + "; vertical-align: " + prefs.alignVertical + "; font-size:" + int2str(prefs.fontSize) + "pt; color:white\">" + int2str(100 - value) + "</td>" + el;
        newRow = newRow + "</tr>" + el;
        out += newRow;
    }
    out = out + "</table> " + el;
    return out;
}