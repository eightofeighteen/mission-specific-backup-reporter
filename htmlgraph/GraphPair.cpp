//
//  GraphPair.cpp
//  HTMLTest
//
//  Created by Stuart Purnell on 2012/09/26.
//  Copyright (c) 2012 Stuart Purnell. All rights reserved.
//

#include "GraphPair.h"

GraphPair::GraphPair() {
    GraphPair("unnamed title", 50);
}

GraphPair::GraphPair(std::string title, int valueP) {
    this->title = title;
    this->valueP = valueP;
}
