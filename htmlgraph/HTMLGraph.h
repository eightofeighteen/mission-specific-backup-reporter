//
//  HTMLGraph.h
//  HTMLTest
//
//  Created by Stuart Purnell on 2012/09/26.
//  Copyright (c) 2012 Stuart Purnell. All rights reserved.
//

#ifndef __HTMLTest__HTMLGraph__
#define __HTMLTest__HTMLGraph__

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <vector>
#include "HTMLGraphPrefs.h"
#include "GraphPair.h"

class HTMLGraph {
public:
    HTMLGraph();
    static std::string outputHTMLGraph(GraphPairList & graphData, HTMLGraphPrefs & prefs);
private:
    static std::string int2str(int num);
    static std::string HTMLHeader();
    static std::string HTMLFooter();
};

#endif /* defined(__HTMLTest__HTMLGraph__) */
