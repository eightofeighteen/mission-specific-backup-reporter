//
//  HTMLGraphPrefs.h
//  HTMLTest
//
//  Created by Stuart Purnell on 2012/09/26.
//  Copyright (c) 2012 Stuart Purnell. All rights reserved.
//

#ifndef __HTMLTest__HTMLGraphPrefs__
#define __HTMLTest__HTMLGraphPrefs__

#include <iostream>

class HTMLGraphPrefs {
public:
    HTMLGraphPrefs();
    HTMLGraphPrefs(std::string font, int fontSize, std::string colorLeft, std::string colorRight, std::string alignVertical, std::string alignHorizontal, std::string alignVerticalLabel, std::string alignHorizontalLabel);
    std::string font;
    int fontSize;
    std::string colorLeft;
    std::string colorRight;
    std::string alignVertical;
    std::string alignHorizontal;
    std::string alignHorizontalLabel;
    std::string alignVerticalLabel;
};

#endif /* defined(__HTMLTest__HTMLGraphPrefs__) */
