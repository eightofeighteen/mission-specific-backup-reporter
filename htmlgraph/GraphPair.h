//
//  GraphPair.h
//  HTMLTest
//
//  Created by Stuart Purnell on 2012/09/26.
//  Copyright (c) 2012 Stuart Purnell. All rights reserved.
//

#ifndef __HTMLTest__GraphPair__
#define __HTMLTest__GraphPair__

#include <iostream>
#include <vector>

class GraphPair {
public:
    GraphPair();
    GraphPair(std::string title, int valueP);
    std::string title;
    int valueP;
};

typedef std::vector<GraphPair> GraphPairList;

#endif /* defined(__HTMLTest__GraphPair__) */
