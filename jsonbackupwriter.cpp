#include "jsonbackupwriter.h"

JsonBackupWriter::JsonBackupWriter() {
    JsonFormat = QJsonDocument::Indented;
}

JsonBackupWriter::JsonBackupWriter(QJsonDocument::JsonFormat jsonFormat) {
    if (jsonFormat == QJsonDocument::Indented || jsonFormat == QJsonDocument::Compact)
        this->JsonFormat = jsonFormat;
    else
        this->JsonFormat = QJsonDocument::Indented;
}

const QByteArray JsonBackupWriter::toJson(QMap<QVariant, QVariant> *aux) const {
    return JsonBackupStatus(aux).toJson(JsonFormat);
}
