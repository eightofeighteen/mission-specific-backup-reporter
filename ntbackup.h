#ifndef NTBACKUP_H
#define NTBACKUP_H
#include "basebackup.h"
#include <QDate>
#include "rmprefs.h"

class NTBackup : public BaseBackup
{
public:
    NTBackup();
    NTBackup(QString name, QString backupLocation, int MinSize);
    QFileInfo backupInfo();
    void updateFileInfo();
    QString toString();
    // 0 passed, 1 file not found, 2 file too small, 3 date wrong
    //enum Status {PASSED, FILENOTFOUND, FILETOOSMALL, FILETOOOLD};
    //Status status;


private:
    QFileInfo _backupInfo;

};

#endif // NTBACKUP_H
