#ifndef OFFLINEFILEINFO_H
#define OFFLINEFILEINFO_H
#include <QString>
#include <QDateTime>
#include <QList>

class OfflineFileInfo;
typedef QList<OfflineFileInfo> OfflineFileInfoList;

class OfflineFileInfo
{
public:
    OfflineFileInfo();
    OfflineFileInfo(QString fileName, QString filePath, QDateTime created, QDateTime lastModified, QDateTime lastAccessed);
    QString fileName();
    QString filePath();
    QDateTime created();
    QDateTime lastModified();
    QDateTime lastAccessed();
    void setFileName(QString fileName);
    void setFilePath(QString filePath);
    void setCreated(QDateTime created);
    void setLastModified(QDateTime lastModified);
    void setLastAccessed(QDateTime lastAccessed);
    const unsigned long long size() const;
    void setSize(unsigned long long);

    static unsigned long long totalSize(const OfflineFileInfoList &list);

private:
    unsigned long long _size;
    QString _fileName;
    QString _filePath;
    QDateTime _created;
    QDateTime _lastModified;
    QDateTime _lastAccessed;
};



#endif // OFFLINEFILEINFO_H
