#ifndef BACKUPLIST_H
#define BACKUPLIST_H
#include <QList>
#include "ntbackup.h"
#include "snapshot.h"
#include "mssqldb.h"
#include "genericfile.h"
#include "backupcollection.h"

//template <class T>
//class BackupCollection;

class BackupList;

template <class T>
class BackupCollection : public QList<T>
{
public:
    BackupCollection(BackupList *parent = 0);

private:
    BackupList *parent;
};


/*!
  Container object for a site.
  Contains information on the site and other containers to store the different backups.
  */
class BackupList
{
public:
    BackupList();
    BackupCollection<NTBackup> NTBackups;  //!< List of NTBackup objects.
    BackupCollection<Snapshot> snapshots;  //!< List of Snapshot objects.
    BackupCollection<MSSQLDB> msdbbackups; //!< List of MSSQLDB objects.
    BackupCollection<GenericFile> genericFileBackups; //!< List of generic file objects.
    void updateFileInfo();
    QString siteName;   //!< Site name.  Used for reporting.
    QMap<QVariant, QVariant> auxName() const;
};


template <class T>
BackupCollection<T>::BackupCollection(BackupList *parent) : QList()
{
    this->parent = parent;
}

#endif // BACKUPLIST_H
