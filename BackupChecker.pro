#-------------------------------------------------
#
# Project created by QtCreator 2012-08-18T10:11:57
#
#-------------------------------------------------

QT       += core

QT       -= gui
QT += network

TARGET = BackupChecker
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    basebackup.cpp \
    ntbackup.cpp \
    snapshot.cpp \
    backuplist.cpp \
    rmprefs.cpp \
    mssqldb.cpp \
    offlinefileinfo.cpp \
    filecountsizepair.cpp \
    robocopybackup.cpp \
    htmlgraph/HTMLGraphPrefs.cpp \
    htmlgraph/HTMLGraph.cpp \
    htmlgraph/GraphPair.cpp \
    genericfile.cpp \
    jsonbackupwriter.cpp \
    backupposter.cpp

HEADERS += \
    basebackup.h \
    ntbackup.h \
    snapshot.h \
    backuplist.h \
    rmprefs.h \
    mssqldb.h \
    offlinefileinfo.h \
    filecountsizepair.h \
    robocopybackup.h \
    htmlgraph/HTMLGraphPrefs.h \
    htmlgraph/HTMLGraph.h \
    htmlgraph/GraphPair.h \
    genericfile.h \
    jsonbackupwriter.h \
    backupposter.h \
    backupcollection.h
